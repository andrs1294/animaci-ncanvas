// create the Scene object

/**
var scene = sjs.Scene({w:640, h:480});

// load the images in parallel. When all the images are
// ready, the callback function is called.
scene.loadImages(['character.png'], function() {

    // create the Sprite object;
    var sp = scene.Sprite('character.png');

    // change the visible size of the sprite
    sp.size(55, 30);

    // apply the latest visual changes to the sprite
    // (draw if canvas, update attribute if DOM);
    sp.update();

    // change the offset of the image in the sprite
    // (this works the opposite way of a CSS background)
    sp.offset(50, 50);

    // various transformations
    sp.move(100, 100);
    sp.rotate(3.14 / 4);
    sp.scale(2);
    sp.setOpacity(0.8);

    sp.update();
});

**/


var canvas = document.getElementById("myCanvas2");
var listGround ;
var listPlant ;
var listTree ;
var listCloud;
var listCow ;
var xIniWorld=600;
var yIniWorld=170;

var canvasWidth = 1062;
var canvasHeight =  532;


var cantidadTerrenoX = 10;
var cantidadTerrenoY = 6;

var weather ;
var farm ;
var cloud ;
//Images for the Ground
var ground1Image = new Image();
ground1Image.src = "assets/images/gallery/terreno/terreno1.png";

var ground2Image = new Image();
ground2Image.src = "assets/images/gallery/terreno/terreno2.png";

var ground3Image = new Image();
ground3Image.src = "assets/images/gallery/terreno/terreno3.png";

//Images for the Plants
var plant1Image = new Image();
plant1Image.src = "assets/images/gallery/plantas/planta1.png";

var plant2Image = new Image();
plant2Image.src = "assets/images/gallery/plantas/planta2.png";

var plant3Image = new Image();
plant3Image.src = "assets/images/gallery/plantas/planta3.png";

var plant4Image = new Image();
plant4Image.src = "assets/images/gallery/plantas/planta4.png";

var plant5Image = new Image();
plant5Image.src = "assets/images/gallery/plantas/planta5.png";

//Images for the Weather
var moonImage = new Image();
moonImage.src = "assets/images/gallery/clima/luna.png";

var cloudyImage = new Image();
cloudyImage.src = "assets/images/gallery/clima/nublado.png";

var sunnyImage = new Image();
sunnyImage.src = "assets/images/gallery/clima/sol.png";

var rainyImage = new Image();
rainyImage.src = "assets/images/gallery/clima/llovizna.png";

//Image for Farm
var farmImage = new Image();
farmImage.src = "assets/images/gallery/otros/decoracion3.png";

//Trees
var tree1Image = new Image();
tree1Image.src = "assets/images/gallery/otros/decoracion6.png";

//Cows
var cowImage = new Image();
cowImage.src = "assets/images/gallery/otros/vaca.png";

//Cloud
var cloudImage = new Image();
cloudImage.src = "assets/images/gallery/otros/nuve.png";


function sprite (options) {

    var that = {};

    that.context = options.context;
    that.width = options.width;
    that.height = options.height;
    that.image = options.image;
    that.complementImage = options.complementImage;
    that.posX = parseInt(options.posX);
    that.posY = parseInt(options.posY);
    that.tamX = parseInt(options.tamX);
    that.tamY = parseInt(options.tamY);
    that.ImageChange=0;
    that.originalImage=options.image;


    that.render = function () {

        // Draw the animation
        that.context.drawImage(
           that.image,
           that.posX,
           that.posY,
           that.tamX,
           that.tamY);
    };


    that.update = function () {

        if(that.ImageChange==0)
        {
            that.image = that.complementImage;
            that.ImageChange=1;

        }else
        {

            that.image = that.originalImage;
            that.ImageChange=0;
            
        }

        

    }; 

    that.updatePos = function () {
        that.posX+=2;
    };

    return that;
}

function createGround(groundImage)
{
    listGround = new Array();

    var xIni=xIniWorld;
    var yIni=yIniWorld;
    var increaseX=34;
    var decreaseX2=increaseX*(cantidadTerrenoY+1);
    var increaseY1=15;
    var decreaseY2=increaseY1*(cantidadTerrenoY-1);

    for(var i=0;i<cantidadTerrenoX;i++)
    {
        for(var j=0;j<cantidadTerrenoY;j++)
        {

           var ground = sprite({
            context: canvas.getContext("2d"),
            width: 159,
            height: 138,
            image: groundImage,
            posX:xIni,
            posY:yIni,
            tamX:82,
            tamY:55
        });

           listGround.push(ground);
           xIni+=increaseX  ;
           yIni+=increaseY1;

       }
       xIni-=decreaseX2;
       yIni-=decreaseY2;
   }
}


function createTrees(treeImage)
{
    listTree = new Array();
    var xIni;
    var yIni;

    xIni=xIniWorld-25;
    yIni=yIniWorld-70;
    
    
    var increaseX=34;
    var decreaseX2=increaseX*(cantidadTerrenoY+1);
    var increaseY1=15;
    var decreaseY2=increaseY1*(cantidadTerrenoY-1);

    for(var i=0;i<cantidadTerrenoX;i++)
    {
        var tree = sprite({
            context: canvas.getContext("2d"),
            width: 159,
            height: 138,
            image: treeImage,
            posX:xIni,
            posY:yIni,
            tamX:50,
            tamY:90
        });

        listTree.push(tree);
        for(var j=0;j<cantidadTerrenoY;j++)
        {

            if(j==cantidadTerrenoY-1)
            {
                var tree = sprite({
                    context: canvas.getContext("2d"),
                    width: 159,
                    height: 138,
                    image: treeImage,
                    posX:xIni+70,
                    posY:yIni+60,
                    tamX:50,
                    tamY:90
                });
                listTree.push(tree);
            }
            xIni+=increaseX  ;
            yIni+=increaseY1;

        }
        xIni-=decreaseX2;
        yIni-=decreaseY2;
    }
}







function drawObject(listObj)
{
    for(var i=0;i<listObj.length;i++)
    {
        listObj[i].render();
    }
}

function createPlant(plantImage,complementImagen)
{
    listPlant = new Array();

    var xIni=xIniWorld+15;
    var yIni=yIniWorld-18;
    var increaseX=34;
    var decreaseX2=increaseX*(cantidadTerrenoY+1);
    var increaseY1=15;
    var decreaseY2=increaseY1*(cantidadTerrenoY-1);

    for(var i=0;i<cantidadTerrenoX;i++)
    {
        for(var j=0;j<cantidadTerrenoY;j++)
        {

           var plant = sprite({
            context: canvas.getContext("2d"),
            width: 159,
            height: 138,
            image: plantImage,
            complementImage: complementImagen,
            posX:xIni,
            posY:yIni,
            tamX:45,
            tamY:40
        });

           listPlant.push(plant);
           xIni+=increaseX  ;
           yIni+=increaseY1;

       }
       xIni-=decreaseX2;
       yIni-=decreaseY2;
   }
}


function createWeather(weatherImage)
{

    var xIni=35;
    var yIni=25;


    weather = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: weatherImage,
        posX:xIni,
        posY:yIni,
        tamX:90,
        tamY:86
    });



   // weather.render();


}


function drawPotato(groundImage,weatherImage)
{
    createGround(groundImage);
    createWeather(weatherImage);
    createFarm();
    drawObject(listGround);
    drawAll(weatherImage);
}

function gameUpdateImages(listObj) {

    for(var i=0;i<listObj.length;i++)
    {
        listObj[i].update();
        listObj[i].render();
    }
    
}

function updatePosCloud()
{
    for(var i=0;i<listCloud.length;i++)
    {
        listObj[i].updatePos();
        listObj[i].render();
    }
}

function drawAll()
{
    canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
    drawObject(listGround);
    drawObject(listTree);
    drawObject(listCow);
    drawObject(listCloud);
    if(listPlant != null)
    {
        drawObject(listPlant);
    }
    
    weather.render();
    farm.render();
}

function createFarm()
{
    var xIni=xIniWorld+(cantidadTerrenoY/2)*34+50;
    var yIni=yIniWorld-100;


    farm = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: farmImage,
        posX:xIni,
        posY:yIni,
        tamX:190,
        tamY:145
    });
}

function createCows()
{
    var xIni=xIniWorld+(cantidadTerrenoY/2)*34+220;
    var yIni=yIniWorld;
    var tamaX=75;
    var tamaY=70;
    listCow = new Array();
    cow1 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: cowImage,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

    xIni+=40;
    yIni+=100;
    cow2 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: cowImage,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

    xIni-=80;
    yIni-=45;
    cow3 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: cowImage,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

    listCow.push(cow1);
    listCow.push(cow2);
    listCow.push(cow3);

}


function createClouds()
{
    var xIni=xIniWorld+(cantidadTerrenoY/2)*34+220;
    var yIni=yIniWorld-150;
    var tamaX=120;
    var tamaY=70;
    listCloud = new Array();
    

    cloud1 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: cloudImage,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

    xIni-=400;
    yIni+=25;
    cloud2 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: cloudImage,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

    xIni-=140;
    yIni-=45;
    cloud3 = sprite({
        context: canvas.getContext("2d"),
        width: 128,
        height: 128,
        image: cloudImage,
        posX:xIni,
        posY:yIni,
        tamX:tamaX,
        tamY:tamaY
    });

    listCloud.push(cloud1);
    listCloud.push(cloud2);
    listCloud.push(cloud3);

}


function createAndDrawTomato(groundImage,plantImage,complementplantImage,weatherImage)
{

    createGround(groundImage);
    createPlant(plantImage,complementplantImage);
    createFarm();
    createCows();
    createTrees(tree1Image);
    createClouds();
    createWeather(weatherImage);
    setInterval(function() { gameUpdateImages(listPlant); },2000);
    setInterval(function() { drawAll(); },2000);

}

$( document ).ready(function() {

    canvas.width = canvasWidth;
    canvas.height = canvasHeight;
    //drawPotato(ground2Image,moonImage);
    createAndDrawTomato(ground1Image,plant2Image,plant3Image,moonImage);
});